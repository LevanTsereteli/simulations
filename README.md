The code in perforning MC simulations of the Chitosan with algirithm described in the following article: https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0180938

Current version of the code is performing MC simulations with only bonded energy (glycosidic PMFs)
Tool can be executed from CMD, with following necessary parameters to be provided: 

 -MonomerNumber             ------------- Number of monomers in the polymer chain
 -Pattern                   ------------- 1 for random 0 for block distribution
 -DD                        ------------- Degree of deacetylation of the polymer (range 0-100%)
 -PH                        ------------- pH of the solution
 -Cs                        ------------- Ionic strength of the solution
 -trajectory                ------------- name of the output trajectory file
 -frequencyOfOutput         ------------- frequency of the output of the trajectory (on every N step)
 -NumberOfSteps             ------------- Total number of the MC steps to be performed 

Example of the cmd line for starting the executable: 

-MonomerNumber  100   -Pattern  1      -DD  0    -PH  4    -Cs  0.1    -trajectory "Trajectory.xyz"       -frequencyOfOutput  100   -NumberOfSteps 1000
