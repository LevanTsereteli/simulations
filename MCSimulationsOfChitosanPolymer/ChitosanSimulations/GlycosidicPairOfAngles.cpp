﻿// This implements the Polysaccharide simulation algorithm discribed in
// https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0180938
// (c) 2020 Levan Tsereteli 
// This code is licensed under MIT license (see LICENSE.txt for details)

#include "GlycosidicPairOfAngles.h"


  int GlycosidicPair::getPMFType () 
   {
     return PMFType; 
   }

  int GlycosidicPair::getBinNumber () 
   {
     return BinNumber; 
   }

  double GlycosidicPair::getFi () 
   {
     return Fi; 
   }

  double GlycosidicPair::getPsi () 
   {
     return Psi; 
   } 

   // Setter 
   void GlycosidicPair::setPMFType (PMF PMFTypeValue) 
   {
      this->PMFType = PMFTypeValue; 
   }

  void GlycosidicPair::setBinNumber (int BinNumberValue) 
   {
      this->BinNumber = BinNumberValue; 
   }

  void GlycosidicPair::setFi (double setFiValue) 
   {
      this->Fi = setFiValue; 
   }

  void GlycosidicPair::setPsi (double setPsiValue) 
   {
      this->Psi = setPsiValue; 
   } 

   // Constructor 
   GlycosidicPair::GlycosidicPair(PMF _PMFType, int _BinNumber, double _Fi, double _Psi)
   {
       this->PMFType = _PMFType;           // Type of the PMF Angle actually belongs to (dinamicaly changing, they are numbered)
       this->BinNumber = _BinNumber;       // Which line in 2D array angle is in ? in which bin
       this->Fi = _Fi;                     // Value of the Fi angle
       this->Psi = _Psi;                   // Value of the Psi angle
   }; 


PMF GlycosidicPair::DetectAngle( Monomer & Monomer1,  Monomer & Monomer2)
{

   PMF PairOfAngles; 

   // int MonomerType {0};             // type of the monomer: 0 - GlcNac, 1 - GlcNH2, 2 - GlcNH3

   // 0
  if (Monomer1.GetMonomerType() == 0 && Monomer2.GetMonomerType() == 0)
   {     
     PairOfAngles = GlcNac_GlcNac; 
   }

   // 1  
   else if (Monomer1.GetMonomerType() == 0 && Monomer2.GetMonomerType() == 1)
   {     
     PairOfAngles = GlcNac_GlcNH2;
   }

   // 2
   else if (Monomer1.GetMonomerType() == 0 && Monomer2.GetMonomerType() == 0)
   {     
     PairOfAngles = GlcNac_GlcNH3;
   }

   // 3
    else if (Monomer1.GetMonomerType() == 0 && Monomer2.GetMonomerType() == 0)
   {     
     PairOfAngles = GlcNH2_GlcNac;
   }

   // 4
    else if (Monomer1.GetMonomerType() == 0 && Monomer2.GetMonomerType() == 0)
   {     
     PairOfAngles = GlcNH2_GlcNH2;
   }

   // 5
    else if (Monomer1.GetMonomerType() == 0 && Monomer2.GetMonomerType() == 0)
   {     
     PairOfAngles = GlcNH2_GlcNH3;
   }

   // 6
    else if (Monomer1.GetMonomerType() == 0 && Monomer2.GetMonomerType() == 0)
   {     
     PairOfAngles = GlcNH3_GlcNac;
   }

   // 7
    else if (Monomer1.GetMonomerType() == 0 && Monomer2.GetMonomerType() == 0)
   {     
     PairOfAngles = GlcNH3_GlcNH2;
   }

   // 8
    else if (Monomer1.GetMonomerType() == 0 && Monomer2.GetMonomerType() == 0)
   {     
     PairOfAngles = GlcNH3_GlcNH3; 
   }

   return PairOfAngles; 

}
    

   GlycosidicPair::GlycosidicPair(){}; 

   // Destructor 
   GlycosidicPair::~GlycosidicPair(){}; 
