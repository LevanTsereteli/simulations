﻿// This implements the Polysaccharide simulation algorithm discribed in
// https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0180938
// (c) 2020 Levan Tsereteli 
// This code is licensed under MIT license (see LICENSE.txt for details)

#pragma once 
#include "Monomer.h"


enum PMF { GlcNac_GlcNac, GlcNac_GlcNH2, GlcNac_GlcNH3, GlcNH2_GlcNac, GlcNH2_GlcNH2, GlcNH2_GlcNH3, GlcNH3_GlcNac, GlcNH3_GlcNH2, GlcNH3_GlcNH3};  // Type of the PMF Angle actually belongs to (dinamicaly changing, they are numbered)


struct GlycosidicPair
{

private: 

  PMF PMFType; 
  // int PMFType;        
  int BinNumber{7640};               // Which line in 2D array angle is in ? in which bin
  double Fi{1.1938042000};           // Value of the Fi angle - First colomn in the PMF
  double Psi{1.2252201000};          // Value of the Psi angle - Second comlomn in the PMF 

public: 
  // Getters 

  int getPMFType ();

  int getBinNumber (); 

  double getFi ();    

  double getPsi ();

  // Setter 
  void setPMFType (PMF PMFTypeValue); 

  void setBinNumber (int BinNumberValue);    

  void setFi (double setFiValue);    

  void setPsi (double setPsiValue);     

  static PMF DetectAngle( Monomer & Monomer1,  Monomer & Monomer2); 

  // Constructor 
  GlycosidicPair(PMF _PMFType, int _BinNumber, double _Fi, double _Psi);
  GlycosidicPair(); 
  // Destructor 
  ~GlycosidicPair(); 

}; 