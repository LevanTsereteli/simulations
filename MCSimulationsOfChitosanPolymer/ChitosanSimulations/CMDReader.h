﻿// This implements the Polysaccharide simulation algorithm discribed in
// https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0180938
// (c) 2020 Levan Tsereteli 
// This code is licensed under MIT license (see LICENSE.txt for details)



#pragma once 
#include <iostream>
#include <stdio.h>
#include <string.h>
#include <cstdint>
#include <cstring>

using namespace std; 

class CMDParams
{
public: 

 int MonomerNumber; 
 bool Pattern; 
 int DD; 
 double PH; 
 double Cs;
 string trajectory; 
 int frequencyOfOutput;
 int NumberOfSteps;  

 // int _MonomerNumber, bool _Pattern, int _DD, double _PH, double _Cs, string _trajectory, int _frequencyOfOutput 

 CMDParams(int _argc, char *argvv[]);   
 CMDParams(); 
~CMDParams(); 

 void ReadCMDParameters(int argc, char *argv[]); 

 void OutputCMDLine(int argc, char *argv[]); 

 void OutputCMDParamsObjectMembersAndAttributes(); 

}; 
