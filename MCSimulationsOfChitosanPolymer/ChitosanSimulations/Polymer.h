﻿// This implements the Polysaccharide simulation algorithm discribed in
// https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0180938
// (c) 2020 Levan Tsereteli 
// This code is licensed under MIT license (see LICENSE.txt for details)

#include "Monomer.h"
#include "GlycosidicPairOfAngles.h"
#include "PolymerStructure.h"
#include "PMFs.h"
#include <iostream>
#include <fstream>      // std::ofstream

class Polymer 
{
   
   public: 
   string trajectory; 
   int frequencyOfOutput; 
   int MN {0};       // number of monomers
   int AN = MN*4;   // number of atoms 1223*4=4892
   std::vector <Monomer> PolymerMonomerChain; 
   std::vector <GlycosidicPair> PolymerGlicLinks; 
   PMFs AllPMFsAndLowEnergyCutOuts; 

   bool RandomPattern {true}; 
   int  DegreeOfDeacetylation {80}; // in percent
     
   int Ph; // sum of protons on the polimer before the titration calculations

   int Ph2; // sum of protons on the polimer after the calculating titration
   			
   double TITR = 0;   // titration energy including neigbours before titration move
   double TITR2;      // titration energy including neigbours after titration
   double Cs = 0.00103162;       // 0.1000316227766017;   // ionic strength of the solution (NaCL)           for PH 4.5 corrction is = 0.0000316227766017
   double PH = 4.5;       // 4.5;         

   double RgirSqr = 0;
   double ETE = 0;
   double METE = 0;
   double RMSETE = 0;
   double LJAfter = 0;  // value of LJ Potential after the structural alteration
   double LJBefore = 0; // value of the LJ Potential Before the structural alteration
   			
   //////////////////
   double EtotalAfter;  // Total energy Bond+Electr+LJ After the structural alteration
   double EtotalBefore; // totall energy Bond+Electr+LJ Before the structural alteration
    			
   double accepted = 0;     // Count how many moves were accepted
   int rejected = 0;        // Count how many moves were rejected
   double acceptance =  0;
   
   double Rgir = 0;
   double CharRatio = 0;
   double CharRatioOO = 0; 
   double PersLength;	

   int count    {0};      // Count of how many times Rgi, ete, rmsete, pers leng and struct are outputed. 
   int countStr {0};    

   // constructors 

   Polymer(); 

   Polymer (int _MonomerNumber, bool _Pattern, int _DD, double _PH, double _Cs, string _trajectory, int _frequencyOfOutput); 
   

	// List all the function needed. 

	// Set size for the Arrays and poplulate them 	 
	
	void PopulatePolymerStructure(); 

	void AssignMonomerTypesWithinChain (); 

	// Update Glycosidic links 
    void UpdateArraysOftheGlycosidicAnglesAndMonomerTypes(); 

    // Do rotation (Pivot Move)
    int PM_Rotate();    
           
	// Output the structure on every N-th move 
    void OutputTrajectory(string FileName); 

    // Calcualte energy of the polymer only bonded 
    double PolymerEnergyPMF(); 

    // Calculate MC Acceptance 
    bool MCAcceptance(double EnergyBefore, double EnergyAfter); 

	// Calculate Rgiration	

}; 
