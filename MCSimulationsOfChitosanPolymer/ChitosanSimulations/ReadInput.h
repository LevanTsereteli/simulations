﻿// This implements the Polysaccharide simulation algorithm discribed in
// https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0180938
// (c) 2020 Levan Tsereteli 
// This code is licensed under MIT license (see LICENSE.txt for details)

#include <iostream>
#include <ctype.h>
using namespace std;

 struct parameters {

 public:
  double pH; 
  double Cs; 
  int    PolLength; 
  int    DD; // in persents 

 public:
  parameters(double a, double b, int c, int d):pH{a},Cs{b},PolLength{c},DD{d}{cout<<"Constructed"<<"\n";
  if (isdigit(a))
  {
   cout<<"Error Input numberic value please"<<"\n";
   exit(1);
  }
  }; 
  ~parameters(){cout<<"Destructred"<<"\n";};

  };

void initialize3 ();

