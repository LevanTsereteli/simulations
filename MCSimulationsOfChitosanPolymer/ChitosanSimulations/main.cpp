﻿// This implements the Polysaccharide simulation algorithm discribed in
// https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0180938
// (c) 2020 Levan Tsereteli 
// This code is licensed under MIT license (see LICENSE.txt for details)

#include <iostream>
#include <ctype.h>
#include "ReadInput.h"
#include "PMFs.h"
#include "GlcNAcGlcNAc.h"
#include <vector>
#include <array>
#include <random>
#include <math.h>       /* exp */
#include <ctime>
#include <fstream>      // std::ofstream
#include <omp.h>
#include <Eigen/Dense>
#include <Eigen/Geometry> 
#include <Eigen/Core>
#include "Polymer.h"
#include "CMDReader.h"

using namespace std;


int main (int argc, char *argv[]) 
{
    ///********* SETTING UP THE SYSTEM (start) **********/// 
    CMDParams CMDParameters; 
    CMDParameters.ReadCMDParameters(argc, argv); 
    cout << "ReadCMDParameters" << endl; 
    CMDParameters.OutputCMDLine(argc, argv); 
    cout << "OutputCMDLine" << endl;
    CMDParameters.OutputCMDParamsObjectMembersAndAttributes();
    cout << "OutputCMDParamsObjectMembersAndAttributes" << endl;
 
    PMFs AllPMFsAndLowEnergyCutOuts; 

    // That is a temporal solution only for GlcNac-GlcNac for which cut off is precalculated
    // here we set the values but reading of all the maps from the file should be implemented 
    AllPMFsAndLowEnergyCutOuts.AllCutOffs.push_back(CutOffArrayVec); 
    AllPMFsAndLowEnergyCutOuts.AllMaps.push_back(Map); 
    // End Set value temp solution 

    Polymer ChitosanPolymer(CMDParameters.MonomerNumber,  CMDParameters.Pattern, CMDParameters.DD,  CMDParameters.PH, CMDParameters.Cs, CMDParameters.trajectory, CMDParameters.frequencyOfOutput);        
    // Polymer ChitosanPolymer(1000, true, 0, 4.5, 0.1, "trajectory.xyz", 100);  
    remove(ChitosanPolymer.trajectory.c_str());
    ChitosanPolymer.PopulatePolymerStructure();
    ChitosanPolymer.AssignMonomerTypesWithinChain(); 
    ChitosanPolymer.UpdateArraysOftheGlycosidicAnglesAndMonomerTypes(); 
    ChitosanPolymer.AllPMFsAndLowEnergyCutOuts =  AllPMFsAndLowEnergyCutOuts; 
    double EnergyPMFPrevious = ChitosanPolymer.PolymerEnergyPMF(); 
    ///********* SETTING UP THE SYSTEM (end)**********///

    for (int t = 0; t < CMDParameters.NumberOfSteps; t++)
      { 
        cout << std::fixed << "Energy PMF before is:  " << EnergyPMFPrevious << endl; 
        Polymer ChitosanPolymerBeforeRotation = ChitosanPolymer; 
        int RotatedGlycosidicAngle = ChitosanPolymer.PM_Rotate();                 
               
        double EnergyPMFAfterRotation = ChitosanPolymer.PolymerEnergyPMF(); 
        cout << std::fixed << "Energy PMF After is:  " << EnergyPMFAfterRotation << endl; 

        bool Acceptance = ChitosanPolymer.MCAcceptance(EnergyPMFPrevious, EnergyPMFAfterRotation);  

        if (Acceptance)
          {
             EnergyPMFPrevious = EnergyPMFAfterRotation; 
             cout << "Accepted" << endl; 
          } 
        else if (!Acceptance)
          {
             cout << "rejected" << endl; 
             ChitosanPolymer = ChitosanPolymerBeforeRotation; 
             // Sample the bin of the map of the non rotated location
             int MapType = ChitosanPolymer.PolymerGlicLinks[RotatedGlycosidicAngle].getPMFType(); 
             int PossitionOnGrid = ChitosanPolymer.PolymerGlicLinks[RotatedGlycosidicAngle].getBinNumber(); 
             ChitosanPolymer.AllPMFsAndLowEnergyCutOuts.AllMaps[MapType][PossitionOnGrid][3] = ChitosanPolymer.AllPMFsAndLowEnergyCutOuts.AllMaps[MapType][PossitionOnGrid][3] + 1; 
          }

          // Here do protonation MC 

          // Then update types of monomers and the glycosidic linkagies 
             ChitosanPolymer.UpdateArraysOftheGlycosidicAnglesAndMonomerTypes(); 

        if (t%ChitosanPolymer.frequencyOfOutput == 0)
          {
             ChitosanPolymer.OutputTrajectory(ChitosanPolymer.trajectory.c_str()); 
          }
      } 
    
    cout << "End of the Program" << endl; 
    exit(0); 

  return 0;
}

