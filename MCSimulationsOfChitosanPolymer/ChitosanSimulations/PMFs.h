﻿// This implements the Polysaccharide simulation algorithm discribed in
// https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0180938
// (c) 2020 Levan Tsereteli 
// This code is licensed under MIT license (see LICENSE.txt for details)

#pragma once 

#include <iostream>
#include <array>
#include <vector>
using namespace std;


//namespace GlycosidicMPFs
//{

class PMFs
{

public: 

// What to read
enum MapsToRead {GlcNac_GlcNac, GlcNac_GlcNH2, GlcNac_GlcNH3, GlcNH2_GlcNac, GlcNH2_GlcNH2, GlcNH2_GlcNH3, GlcNH3_GlcNac, GlcNH3_GlcNH2, GlcNH3_GlcNH3};  

vector<int>  CutOffIndiceArrayGlcNac_GlcNac;
vector<int>  CutOffIndiceArrayGlcNac_GlcNH2; 
vector<int>  CutOffIndiceArrayGlcNac_GlcNH3; 
vector<int>  CutOffIndiceArrayGlcNH2_GlcNac;
vector<int>  CutOffIndiceArrayGlcNH2_GlcNH2;
vector<int>  CutOffIndiceArrayGlcNH2_GlcNH3; 
vector<int>  CutOffIndiceArrayGlcNH3_GlcNac; 
vector<int>  CutOffIndiceArrayGlcNH3_GlcNH2; 
vector<int>  CutOffIndiceArrayGlcNH3_GlcNH3; 

vector<vector<int>> AllCutOffs; 


// Memory leak manage it other way, calculate the size // 
std::array<std::array <double, 4>, 40000>   GlcNac_GlcNacMap; 
//std::array<std::array <double, 4>, 40000>   GlcNac_GlcNH2Map; 
//std::array<std::array <double, 4>, 40000>   GlcNac_GlcNH3Map; 
//std::array<std::array <double, 4>, 40000>   GlcNH2_GlcNacMap; 
//std::array<std::array <double, 4>, 40000>   GlcNH2_GlcNH2Map; 
//std::array<std::array <double, 4>, 40000>   GlcNH2_GlcNH3Map; 
//std::array<std::array <double, 4>, 40000>   GlcNH3_GlcNacMap;
//std::array<std::array <double, 4>, 40000>   GlcNH3_GlcNH2Map; 
//std::array<std::array <double, 4>, 40000>   GlcNH3_GlcNH3Map;  

vector<std::array<std::array <double, 4>, 40000>> AllMaps; 

void ReadPMFs(); 
void CutOffArrayOfBinNumbersOfPMFs(double CutOffEnergy);   // CutOffEnergy taken from the CMD 
PMFs(); 
~PMFs(); 

}; 
