﻿// This implements the Polysaccharide simulation algorithm discribed in
// https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0180938
// (c) 2020 Levan Tsereteli 
// This code is licensed under MIT license (see LICENSE.txt for details)

#include "ReadInput.h"
#include <iostream>
#include <fstream> 
#include <ctype.h>
using namespace std;


void initialize3 () 
{


 
   cout << "Please Enter value in correct units, or it will not build:" <<"\n"<< endl;
   

    parameters x (1,1,1,1);
	cout << "1 - Enter pH of the system(floating point):" <<"\n"<< endl;

 	cin>>x.pH;

 	while(std::cin.fail()) {
        std::cout << "Error - Enter Proper Input" << std::endl;
        std::cin.clear();
        std::cin.ignore(256,'\n');
        std::cin >> x.pH;
    }

 	cout << "2 - Enter Cs of the system(floating point):" <<"\n"<< endl;

 	cin>>x.Cs; 

 	while(std::cin.fail()) {
        std::cout << "Error - Enter Proper Input" << std::endl;
        std::cin.clear();
        std::cin.ignore(256,'\n');
        std::cin >> x.Cs;
    }

 	cout << "3 - Enter PolLength of the system(interger):" <<"\n"<< endl;

 	cin>>x.PolLength;

 	while(std::cin.fail()) {
        std::cout << "Error - Enter Proper Input" << std::endl;
        std::cin.clear();
        std::cin.ignore(256,'\n');
        std::cin >> x.PolLength;
    }

 	cout << "4 - Enter Degree of Deacetylation of the polymer(integer point, persents):" <<"\n"<< endl;

 	cin>>x.DD; 

 	while(std::cin.fail()) {
        std::cout << "Error - Enter Proper Input" << std::endl;
        std::cin.clear();
        std::cin.ignore(256,'\n');
        std::cin >> x.DD;
    }


	cout<<x.pH<<"\n";
	cout<<x.Cs<<"\n";
	cout<<x.PolLength<<"\n";
	cout<<x.DD<<"\n"; 

}
