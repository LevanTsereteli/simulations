﻿// This implements the Polysaccharide simulation algorithm discribed in
// https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0180938
// (c) 2020 Levan Tsereteli 
// This code is licensed under MIT license (see LICENSE.txt for details)

#pragma once 
#include <Eigen/Dense>


  // Monomer Structures 
  struct CGMonomerStructure
   {
   public: 
      Eigen::Vector3d O4; 
      Eigen::Vector3d C1; 
      Eigen::Vector3d CG; 
      Eigen::Vector3d C4;  

      CGMonomerStructure(Eigen::Vector3d O4a, Eigen::Vector3d C1a, Eigen::Vector3d CGa, Eigen::Vector3d C4a)
      {
        this->O4 = O4a; 
        this->C1 = C1a; 
        this->CG = CGa; 
        this->C4 = C4a; 
      }; 

        CGMonomerStructure(){}; 
       ~CGMonomerStructure(){}; 
   }; 

 
  
   // To be done 
  struct AAMonomerStucture
   {
    //  Eigen::Vector3d O4; 
    //  Eigen::Vector3d C1; 
    //  Eigen::Vect#pragma once or3d CG; 
    //  Eigen::Vector3d C4;  
    //  Eigen::Vector3d O4; 
    //  Eigen::Vector3d C1; 
    //  Eigen::Vector3d CG; 
    //  Eigen::Vector3d C4;  
   }; 



class Monomer 

{

 private: 

   double Pkint {6.6};              // Value of the intrinsic PK 
   int ProtonationState {1};        // Is it protonated or not 
   int MonomerType {0};             // type of the monomer: 0 - GlcNac, 1 - GlcNH2, 2 - GlcNH3

 public: 

   CGMonomerStructure CGPolymerChain; 
   AAMonomerStucture  AAPolymerChain; 

   // Getters 
   int GetMonomerType(); 
   int GetProtonationState(); 
   double GetPkint(); 

   // Setters
   void SetMonomerType(const int & MonType); 

   void SetProtonationState(const int & ProtState); 

   void SetPkint(const double & Pk); 

   // Constructor 
   Monomer(double _Pkint, int _ProtonationState, int _MonomerType); 
   Monomer(double _Pkint, int _ProtonationState, int _MonomerType, CGMonomerStructure _CGStructure); 
   Monomer(); 
   // Destructor 
   ~Monomer(); 
}; 
