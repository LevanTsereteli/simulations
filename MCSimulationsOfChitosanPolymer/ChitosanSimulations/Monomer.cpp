﻿// This implements the Polysaccharide simulation algorithm discribed in
// https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0180938
// (c) 2020 Levan Tsereteli 
// This code is licensed under MIT license (see LICENSE.txt for details)

#include "Monomer.h"


   int Monomer::GetMonomerType()
   {
     return this->MonomerType; 
   }

   int Monomer::GetProtonationState()
   {
     return this->ProtonationState; 
   }

   double Monomer::GetPkint()
   {
     return this->Pkint; 
   }

   // Setters
   void Monomer::SetMonomerType(const int & MonType)
   {
      this->MonomerType = MonType; 
   }

   void Monomer::SetProtonationState(const int & ProtState)
   {
      this->ProtonationState = ProtState; 
   }

   void Monomer::SetPkint(const double & Pk)
   {
      this->Pkint = Pk; 
   }

   // Constructor 
   Monomer::Monomer (double _Pkint, int _ProtonationState, int _MonomerType) 
   {
      this->Pkint = _Pkint;                         // Value of the intrinsic PK 
      this->ProtonationState = _ProtonationState;   // Is it protonated or not 
      this->MonomerType = _MonomerType;             // type of the monomer: 0 - GlcNac, 1 - GlcNH2, 2 - GlcNH3
   };


   Monomer::Monomer(double _Pkint, int _ProtonationState, int _MonomerType, CGMonomerStructure _CGStructure)
   {
       this->Pkint = _Pkint;                         // Value of the intrinsic PK 
       this->ProtonationState = _ProtonationState;   // Is it protonated or not 
       this->MonomerType = _MonomerType;             // type of the monomer: 0 - GlcNac, 1 - GlcNH2, 2 - GlcNH3
       this->CGPolymerChain = _CGStructure; 
   }

   // Defult constructor      
   Monomer::Monomer(){};
   // Destructor 
   Monomer::~Monomer(){};
