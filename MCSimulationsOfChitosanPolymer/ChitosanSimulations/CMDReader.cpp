﻿// This implements the Polysaccharide simulation algorithm discribed in
// https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0180938
// (c) 2020 Levan Tsereteli 
// This code is licensed under MIT license (see LICENSE.txt for details)

#include "CMDReader.h"

  
 CMDParams::CMDParams(){}; 

 CMDParams::~CMDParams(){};


 void CMDParams::ReadCMDParameters(int argc, char *argv[])
 {
     for (int IterCMD = 0; IterCMD < argc; IterCMD++) 
     {
        if (strcmp(argv[IterCMD], "-MonomerNumber") == 0)
        {
          this->MonomerNumber = std::atoi(argv[IterCMD + 1]); 
        } 

        if (strcmp(argv[IterCMD],"-Pattern") == 0)
        {

          int pat = std::atoi(argv[IterCMD + 1]); 

          if(pat == 1)
          {
              this->Pattern = true;
          }
          else if(pat == 0)
          {
              this->Pattern = false;
          }

        } 

        if (strcmp(argv[IterCMD],"-DD") == 0)
        {
          this->DD = std::atoi(argv[IterCMD + 1]); 
        }

        if (strcmp(argv[IterCMD],"-PH") == 0)
        {
          this->PH = std::stod(argv[IterCMD + 1]); 
        }

        if (strcmp(argv[IterCMD],"-Cs") == 0)
        {
          this->Cs = std::stod(argv[IterCMD + 1]); 
        }

        if (strcmp(argv[IterCMD],"-trajectory") == 0)
        {
          this->trajectory = argv[IterCMD + 1]; 
        }

        if (strcmp(argv[IterCMD],"-frequencyOfOutput") == 0)
        {
          this->frequencyOfOutput = std::atoi(argv[IterCMD + 1]); 
        }

        if (strcmp(argv[IterCMD],"-NumberOfSteps") == 0)
        {
          this->NumberOfSteps = std::atoi(argv[IterCMD + 1]); 
        }
     }
 }


void CMDParams::OutputCMDLine(int argc, char *argv[])
 {
   for (int IterCMD = 0; IterCMD < argc; IterCMD++) 
   {
      cout << argv[IterCMD] << endl; 
   }  
 } 


void CMDParams::OutputCMDParamsObjectMembersAndAttributes()
 {
    cout <<  "MonomerNumber    "<< this->MonomerNumber << endl;  
    cout <<  "Pattern    "<< this->Pattern << endl; 
    cout <<  "DD    "<< this->DD << endl; 
    cout <<  "PH    "<< this->PH << endl; 
    cout <<  "Cs    "<< this->Cs << endl;
    cout <<  "trajectory    "<< this->trajectory << endl;  
    cout <<  "frequencyOfOutput    "<< this->frequencyOfOutput << endl;
    cout <<  "NumberOfSteps    "<< this->NumberOfSteps << endl;
 }

  // -MonomerNumber  100   -Pattern  1      -DD  0    -PH  4    -Cs  0.1    -trajectory  "Trajectory.xyz"       -frequencyOfOutput  100   -NumberOfSteps 20000
